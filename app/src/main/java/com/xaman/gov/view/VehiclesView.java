package com.xaman.gov.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;

import com.xaman.gov.R;

/**
 * Created by monica on 12/29/17.
 */

public class VehiclesView extends View {
    private String vehicleName;
    private String vehicleConfidence;
    private int borderColor;
    private float borderThickness;
    private int startX;
    private int startY;
    private int endX;
    private int endY;

    public VehiclesView(Context context) {
        super(context);
    }

    public VehiclesView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context, attrs);
    }

    private void initView(Context context, AttributeSet attrs) {
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs,
                R.styleable.VehiclesView, 0, 0);
        vehicleName = a.getString(R.styleable.VehiclesView_vehicleName);
        vehicleConfidence = a.getString(R.styleable.VehiclesView_vehicleConfidence);
        borderColor = a.getColor(R.styleable.VehiclesView_borderColor, 0);
        borderThickness = a.getInt(R.styleable.VehiclesView_borderThickness, 0);
        startX = a.getInt(R.styleable.VehiclesView_startX, 0);
        startY = a.getInt(R.styleable.VehiclesView_startY, 0);
        endX = a.getInt(R.styleable.VehiclesView_endX, 0);
        endY = a.getInt(R.styleable.VehiclesView_endY, 0);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Paint paint = new Paint();
        paint.setColor(ContextCompat.getColor(getContext() ,R.color.colorAccent));
        paint.setStrokeWidth(3);
        paint.setStyle(Paint.Style.STROKE);
        int width = endX - startX;
        int height = endY - startY;
        /*Bitmap.Config conf = Bitmap.Config.ARGB_4444; // see other conf types
        Bitmap bmp = Bitmap.createBitmap(width, height, conf);*/
        Rect rect = new Rect();
        rect.set(startX, startY, endX, endY);
        canvas.drawRect(rect, paint);
    }

    public String getVehicleName() {
        return vehicleName;
    }

    public void setVehicleName(String vehicleName) {
        this.vehicleName = vehicleName;
    }

    public String getVehicleConfidence() {
        return vehicleConfidence;
    }

    public void setVehicleConfidence(String vehicleConfidence) {
        this.vehicleConfidence = vehicleConfidence;
    }

    public int getBorderColor() {
        return borderColor;
    }

    public void setBorderColor(int borderColor) {
        this.borderColor = borderColor;
    }

    public float getBorderThickness() {
        return borderThickness;
    }

    public void setBorderThickness(float borderThickness) {
        this.borderThickness = borderThickness;
    }

    public int getStartX() {
        return startX;
    }

    public void setStartX(int startX) {
        this.startX = startX;
    }

    public int getStartY() {
        return startY;
    }

    public void setStartY(int startY) {
        this.startY = startY;
    }

    public int getEndX() {
        return endX;
    }

    public void setEndX(int endX) {
        this.endX = endX;
    }

    public int getEndY() {
        return endY;
    }

    public void setEndY(int endY) {
        this.endY = endY;
    }

}
