package com.xaman.gov.camera;

import android.content.Context;

import com.xaman.gov.models.TrafficStatus;
import com.xaman.gov.models.VehicleAnnotation;

import java.io.File;

/**
 * Created by monica on 12/28/17.
 */

public interface TrafficUpdateView {
    void trafficUpdated(TrafficStatus status, File file);
    void trafficUpdatedFailed(String error, Throwable e);
    Context getContext();
    void renderVehicleAnnotation(VehicleAnnotation vehicleAnnotation);
    void finishRenderingVehicles();
    void forgetDetectedVehicles();
}
