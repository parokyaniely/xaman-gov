package com.xaman.gov.camera;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Size;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.widget.Toast;

import com.xaman.gov.BuildConfig;
import com.xaman.gov.R;
import com.xaman.gov.databinding.ActivityMainBinding;
import com.xaman.gov.models.TrafficStatus;
import com.xaman.gov.models.VehicleAnnotation;
import com.xaman.gov.view.VehiclesView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;

public class MainActivity extends AppCompatActivity implements TextureView.SurfaceTextureListener, TrafficUpdateView{

    CameraManager cameraManager;
    int cameraPers;
    Size previewSize;
    private String cameraId;
    private CameraDevice cameraDevice;
    private ActivityMainBinding binding;
    private HandlerThread backgroundThread;
    private Handler handler;
    private CaptureRequest.Builder captureRequestBuilder;
    private CaptureRequest captureRequest;
    private CameraCaptureSession cameraCaptureSession;
    private CameraDevice.StateCallback stateCallback;
    private UpdateTrafficPresenter presenter;
    private String fileAbsolutePath;
    private ArrayList<VehiclesView> detectedVehicles;
    private int countdown = 30;

    /**TODO: Send stream requests to server
     * TODO: Update UI*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        detectedVehicles = new ArrayList<>();
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        checkIfCameraIsOperational();
        initStateCallback();
        presenter = new UpdateTrafficPresenter(this);
        updateCountdown();
    }

    private void updateCountdown() {
        getSupportActionBar().setTitle(getString(R.string.app_name) +" at " + BuildConfig.INTERSECTION_ID + " " + BuildConfig.DIRECTION_NAME + "("+countdown+")");
    }

    private void initStateCallback() {
        stateCallback = new CameraDevice.StateCallback() {
            @Override
            public void onOpened(@NonNull CameraDevice camera) {
                MainActivity.this.cameraDevice = camera;
                createPreviewSession();
            }

            @Override
            public void onDisconnected(@NonNull CameraDevice camera) {
                camera.close();
                cameraDevice = null;
            }

            @Override
            public void onError(@NonNull CameraDevice camera, int error) {
                camera.close();
                cameraDevice = null;
            }
        };
    }

    @Override
    protected void onResume() {
        super.onResume();
        openBackgroundThread();
        if (binding.cameraView.isAvailable()) {
            setUpCamera();
            openCamera();
        } else {
            binding.cameraView.setSurfaceTextureListener(this);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        closeCamera();
        closeBackgroundThread();
    }

    private void createPreviewSession() {
        SurfaceTexture texture = binding.cameraView.getSurfaceTexture();
        texture.setDefaultBufferSize(previewSize.getWidth(), previewSize.getHeight());
        Surface previewSurface = new Surface(texture);
        try {
            captureRequestBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            captureRequestBuilder.addTarget(previewSurface);
            cameraDevice.createCaptureSession(Collections.singletonList(previewSurface),
                    new CameraCaptureSession.StateCallback() {
                        @Override
                        public void onConfigured(@NonNull CameraCaptureSession session) {
                            if (cameraDevice == null) {
                                return;
                            }
                            captureRequest = captureRequestBuilder.build();
                            cameraCaptureSession = session;
                            try {
                                cameraCaptureSession.setRepeatingRequest(captureRequest, null, handler);
                                takePicture();
                            } catch (CameraAccessException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onConfigureFailed(@NonNull CameraCaptureSession session) {

                        }
                    }, handler);

        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void takePicture() {
        savePicture();
    }

    private void savePicture() {
        String filename = "stream";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-mm-dd-kk:mm:ss");
        String dateString = new String();
        try {
            dateString = simpleDateFormat.format(Calendar.getInstance().getTime());
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        filename = filename + dateString;
        String fileDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getAbsolutePath();
        FileOutputStream outputStream = null;
        try {
            File file = new File(fileDirectory + "/" + filename + ".jpg");
            outputStream = new FileOutputStream(file);
            Bitmap pic = binding.cameraView.getBitmap();
            fileAbsolutePath = file.getAbsolutePath();
            pic.compress(Bitmap.CompressFormat.JPEG, 90, outputStream);

            outputStream.flush();
            outputStream.close();
            setSnapshot();
            presenter.updateTraffic(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            startTimer();
        } catch (IOException e) {
            e.printStackTrace();
            startTimer();
        }
    }

    private void setSnapshot() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                BitmapFactory.Options options = new BitmapFactory.Options();
                Bitmap bitmap = BitmapFactory.decodeFile(fileAbsolutePath, options);
                binding.ivSnapshot.setVisibility(View.VISIBLE);
                bitmap = Bitmap.createScaledBitmap(bitmap,
                        binding.getRoot().getWidth(), binding.getRoot().getHeight(), true);
                binding.ivSnapshot.setImageBitmap(bitmap);
            }
        });
    }

    private void clearSnapshot() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                binding.ivSnapshot
                        .setImageDrawable(getDrawable(android.R.drawable.screen_background_dark_transparent));
                binding.ivSnapshot.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void trafficUpdated(TrafficStatus status, File file) {
        if (file.exists()) {
            file.delete();
        }
        Toast.makeText(this, "Vehicles detected: " + status.getNumDetected(), Toast.LENGTH_SHORT).show();
        presenter.renderDetectedVehicles(status.getDetectionData().getObjects());
    }

    @Override
    public void trafficUpdatedFailed(String error, Throwable e) {
        if (error != null) {
            Log.d(MainActivity.class.getSimpleName(), error);
            Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
        } else {
            Log.d(MainActivity.class.getSimpleName(), "Unknown error");
        }
        clearSnapshot();
        startTimer();
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void renderVehicleAnnotation(VehicleAnnotation vehicleAnnotation) {
        VehiclesView vehiclesView = new VehiclesView(this);
        vehiclesView.setStartX(vehicleAnnotation.getBounding().getVertices().get(0).getX());
        vehiclesView.setEndX(vehicleAnnotation.getBounding().getVertices().get(2).getX());
        vehiclesView.setStartY(vehicleAnnotation.getBounding().getVertices().get(0).getY());
        vehiclesView.setEndY(vehicleAnnotation.getBounding().getVertices().get(2).getY());
        ((ConstraintLayout) binding.getRoot()).addView(vehiclesView);
        detectedVehicles.add(vehiclesView);
    }

    @Override
    public void finishRenderingVehicles() {
        startWaitTimer();
    }

    private void startWaitTimer() {
        CountDownTimer timer = new CountDownTimer(5000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                clearSnapshot();
                forgetDetectedVehicles();
                startTimer();
            }
        }.start();
    }

    @Override
    public void forgetDetectedVehicles() {
        for (VehiclesView vehiclesView : detectedVehicles) {
            ((ConstraintLayout) binding.getRoot()).removeView(vehiclesView);
        }
        detectedVehicles.clear();
    }

    private void startTimer() {
        updateCountdown();
        final CountDownTimer countDownTimer = new CountDownTimer(30000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                countdown--;
                updateCountdown();
            }

            @Override
            public void onFinish() {
                savePicture();
                countdown = 30;
                Toast.makeText(MainActivity.this, "Updating....", Toast.LENGTH_SHORT).show();
            }
        }.start();
    }

    private void closeCamera() {

    }

    private void closeBackgroundThread() {
        if (handler != null) {
            backgroundThread.quitSafely();
            backgroundThread = null;
            handler = null;
        }
    }

    private void checkIfCameraIsOperational() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 1);
        cameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        cameraPers = CameraCharacteristics.LENS_FACING_BACK;
    }

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
        setUpCamera();
        openCamera();
    }

    private void setUpCamera() {
        try {
            for (String cameraId : cameraManager.getCameraIdList()) {
                CameraCharacteristics characteristics = cameraManager.getCameraCharacteristics(cameraId);
                if (characteristics.get(CameraCharacteristics.LENS_FACING) == cameraPers) {
                    StreamConfigurationMap configurationMap = characteristics
                            .get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
                    previewSize = configurationMap.getOutputSizes(SurfaceTexture.class)[0];
                    this.cameraId = cameraId;
                }
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void openCamera() {
        try {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA)
                    == PackageManager.PERMISSION_GRANTED) {
                cameraManager.openCamera(cameraId, stateCallback, handler);
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    private void openBackgroundThread() {
        backgroundThread = new HandlerThread("camera_background_thread");
        backgroundThread.start();
        handler = new Handler(backgroundThread.getLooper());
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {

    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
        return false;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {

    }
}