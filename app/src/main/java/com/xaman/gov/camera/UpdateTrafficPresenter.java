package com.xaman.gov.camera;

import com.xaman.gov.BuildConfig;
import com.xaman.gov.models.TrafficObject;
import com.xaman.gov.models.TrafficStatus;
import com.xaman.gov.network.XamanRetrofit;
import com.xaman.gov.network.XamanService;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by monica on 12/28/17.
 */

public class UpdateTrafficPresenter {
    TrafficUpdateView view;
    XamanRetrofit xamanRetrofit;
    XamanService service;

    public UpdateTrafficPresenter(TrafficUpdateView view) {
        this.view = view;
        xamanRetrofit = new XamanRetrofit();
        service = xamanRetrofit.getXamanRetrofit().create(XamanService.class);
    }

    public void updateTraffic(final File file) {
        service.updateTrafficStatus(assembleTrafficRequestBody(), prepareFile(file)).enqueue(new Callback<TrafficStatus>() {
            @Override
            public void onResponse(Call<TrafficStatus> call, Response<TrafficStatus> response) {
                if (response.code() == 200) {
                    view.trafficUpdated(response.body(), file);
                } else {
                    try {
                        view.trafficUpdatedFailed(response.errorBody().string(), new Exception());
                    } catch (IOException e) {
                        e.printStackTrace();
                        view.trafficUpdatedFailed(e.getMessage(), e);
                    }
                }
            }

            @Override
            public void onFailure(Call<TrafficStatus> call, Throwable t) {
                view.trafficUpdatedFailed(t.getMessage(), t);
            }
        });
    }

    private MultipartBody.Part prepareFile(File file) {
        RequestBody requestBody = RequestBody.create(MediaType.parse("image/*"), file);
        return MultipartBody.Part.createFormData("image", file.getName(), requestBody);
    }

    private Map<String,RequestBody> assembleTrafficRequestBody() {
        Map<String, RequestBody> map = new HashMap<>();
//        map.put("image", RequestBody.create(MediaType.parse("image/*"), new File(file.getAbsolutePath())));
        map.put("intersection_id", RequestBody.create(MediaType.parse("text/plain"), BuildConfig.INTERSECTION_ID));
        map.put("direction_name", RequestBody.create(MediaType.parse("text/plain"), BuildConfig.DIRECTION_NAME));
        return map;
    }

    public void renderDetectedVehicles(ArrayList<TrafficObject> objects) {
        for (TrafficObject trafficObject : objects) {
            view.renderVehicleAnnotation(trafficObject.getVehicleAnnotation());
        }
        view.finishRenderingVehicles();
    }
}
