package com.xaman.gov.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by monica on 12/29/17.
 */

public class TrafficObject implements Parcelable {
    @SerializedName("objectId")
    @Expose
    public String objectId;
    @SerializedName("vehicleAnnotation")
    @Expose
    public VehicleAnnotation vehicleAnnotation;
    @SerializedName("objectType")
    @Expose
    public String objectType;

    protected TrafficObject(Parcel in) {
        objectId = in.readString();
        vehicleAnnotation = in.readParcelable(VehicleAnnotation.class.getClassLoader());
        objectType = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(objectId);
        dest.writeParcelable(vehicleAnnotation, flags);
        dest.writeString(objectType);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<TrafficObject> CREATOR = new Creator<TrafficObject>() {
        @Override
        public TrafficObject createFromParcel(Parcel in) {
            return new TrafficObject(in);
        }

        @Override
        public TrafficObject[] newArray(int size) {
            return new TrafficObject[size];
        }
    };

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public VehicleAnnotation getVehicleAnnotation() {
        return vehicleAnnotation;
    }

    public void setVehicleAnnotation(VehicleAnnotation vehicleAnnotation) {
        this.vehicleAnnotation = vehicleAnnotation;
    }

    public String getObjectType() {
        return objectType;
    }

    public void setObjectType(String objectType) {
        this.objectType = objectType;
    }
}
