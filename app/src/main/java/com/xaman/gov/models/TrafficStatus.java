package com.xaman.gov.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by monica on 12/28/17.
 */
public class TrafficStatus implements Parcelable {
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("num_detected")
    @Expose
    public Integer numDetected;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("detection_data")
    @Expose
    public DetectionData detectionData;

    protected TrafficStatus(Parcel in) {
        status = in.readString();
        if (in.readByte() == 0) {
            numDetected = null;
        } else {
            numDetected = in.readInt();
        }
        image = in.readString();
        detectionData = in.readParcelable(DetectionData.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(status);
        if (numDetected == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(numDetected);
        }
        dest.writeString(image);
        dest.writeParcelable(detectionData, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<TrafficStatus> CREATOR = new Creator<TrafficStatus>() {
        @Override
        public TrafficStatus createFromParcel(Parcel in) {
            return new TrafficStatus(in);
        }

        @Override
        public TrafficStatus[] newArray(int size) {
            return new TrafficStatus[size];
        }
    };

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getNumDetected() {
        return numDetected;
    }

    public void setNumDetected(Integer numDetected) {
        this.numDetected = numDetected;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public DetectionData getDetectionData() {
        return detectionData;
    }

    public void setDetectionData(DetectionData detectionData) {
        this.detectionData = detectionData;
    }
}
