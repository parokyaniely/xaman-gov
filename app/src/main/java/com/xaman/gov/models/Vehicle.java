package com.xaman.gov.models;

import android.graphics.Color;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by monica on 12/29/17.
 */

public class Vehicle implements Parcelable {
    @SerializedName("make")
    @Expose
    public Make make;
    @SerializedName("model")
    @Expose
    public VehicleModel model;
    @SerializedName("color")
    @Expose
    public VehicleColor color;
    @SerializedName("vehicleType")
    @Expose
    public String vehicleType;

    protected Vehicle(Parcel in) {
        make = in.readParcelable(Make.class.getClassLoader());
        model = in.readParcelable(VehicleModel.class.getClassLoader());
        color = in.readParcelable(VehicleColor.class.getClassLoader());
        vehicleType = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(make, flags);
        dest.writeParcelable(model, flags);
        dest.writeParcelable(color, flags);
        dest.writeString(vehicleType);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Vehicle> CREATOR = new Creator<Vehicle>() {
        @Override
        public Vehicle createFromParcel(Parcel in) {
            return new Vehicle(in);
        }

        @Override
        public Vehicle[] newArray(int size) {
            return new Vehicle[size];
        }
    };
}
