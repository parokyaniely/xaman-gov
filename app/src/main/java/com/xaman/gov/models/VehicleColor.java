package com.xaman.gov.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by monica on 12/29/17.
 */

public class VehicleColor implements Parcelable {

    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("confidence")
    @Expose
    public Double confidence;

    protected VehicleColor(Parcel in) {
        name = in.readString();
        if (in.readByte() == 0) {
            confidence = null;
        } else {
            confidence = in.readDouble();
        }
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        if (confidence == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(confidence);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<VehicleColor> CREATOR = new Creator<VehicleColor>() {
        @Override
        public VehicleColor createFromParcel(Parcel in) {
            return new VehicleColor(in);
        }

        @Override
        public VehicleColor[] newArray(int size) {
            return new VehicleColor[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getConfidence() {
        return confidence;
    }

    public void setConfidence(Double confidence) {
        this.confidence = confidence;
    }
}
