package com.xaman.gov.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by monica on 12/29/17.
 */

public class Make implements Parcelable {
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("confidence")
    @Expose
    public Double confidence;

    protected Make(Parcel in) {
        name = in.readString();
        if (in.readByte() == 0) {
            confidence = null;
        } else {
            confidence = in.readDouble();
        }
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        if (confidence == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(confidence);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Make> CREATOR = new Creator<Make>() {
        @Override
        public Make createFromParcel(Parcel in) {
            return new Make(in);
        }

        @Override
        public Make[] newArray(int size) {
            return new Make[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getConfidence() {
        return confidence;
    }

    public void setConfidence(Double confidence) {
        this.confidence = confidence;
    }
}
