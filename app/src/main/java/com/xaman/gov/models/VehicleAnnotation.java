package com.xaman.gov.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by monica on 12/29/17.
 */

public class VehicleAnnotation implements Parcelable {
    @SerializedName("bounding")
    @Expose
    public Bounding bounding;
    @SerializedName("recognitionConfidence")
    @Expose
    public Double recognitionConfidence;
    @SerializedName("attributes")
    @Expose
    public Attributes attributes;

    protected VehicleAnnotation(Parcel in) {
        bounding = in.readParcelable(Bounding.class.getClassLoader());
        if (in.readByte() == 0) {
            recognitionConfidence = null;
        } else {
            recognitionConfidence = in.readDouble();
        }
        attributes = in.readParcelable(Attributes.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(bounding, flags);
        if (recognitionConfidence == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(recognitionConfidence);
        }
        dest.writeParcelable(attributes, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<VehicleAnnotation> CREATOR = new Creator<VehicleAnnotation>() {
        @Override
        public VehicleAnnotation createFromParcel(Parcel in) {
            return new VehicleAnnotation(in);
        }

        @Override
        public VehicleAnnotation[] newArray(int size) {
            return new VehicleAnnotation[size];
        }
    };

    public Bounding getBounding() {
        return bounding;
    }

    public void setBounding(Bounding bounding) {
        this.bounding = bounding;
    }

    public Double getRecognitionConfidence() {
        return recognitionConfidence;
    }

    public void setRecognitionConfidence(Double recognitionConfidence) {
        this.recognitionConfidence = recognitionConfidence;
    }

    public Attributes getAttributes() {
        return attributes;
    }

    public void setAttributes(Attributes attributes) {
        this.attributes = attributes;
    }
}
