package com.xaman.gov.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by monica on 12/29/17.
 */

public class Attributes implements Parcelable {

    @SerializedName("system")
    @Expose
    public Vehicle system;

    protected Attributes(Parcel in) {
        system = in.readParcelable(Vehicle.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(system, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Attributes> CREATOR = new Creator<Attributes>() {
        @Override
        public Attributes createFromParcel(Parcel in) {
            return new Attributes(in);
        }

        @Override
        public Attributes[] newArray(int size) {
            return new Attributes[size];
        }
    };

    public Vehicle getSystem() {
        return system;
    }

    public void setSystem(Vehicle system) {
        this.system = system;
    }
}
