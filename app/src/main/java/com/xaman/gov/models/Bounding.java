package com.xaman.gov.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by monica on 12/29/17.
 */

public class Bounding implements Parcelable {
    @SerializedName("vertices")
    @Expose
    public ArrayList<Vertex> vertices;

    public Bounding() {
        vertices = new ArrayList<>();
    }

    protected Bounding(Parcel in) {
        vertices = in.createTypedArrayList(Vertex.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(vertices);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Bounding> CREATOR = new Creator<Bounding>() {
        @Override
        public Bounding createFromParcel(Parcel in) {
            return new Bounding(in);
        }

        @Override
        public Bounding[] newArray(int size) {
            return new Bounding[size];
        }
    };

    public ArrayList<Vertex> getVertices() {
        return vertices;
    }

    public void setVertices(ArrayList<Vertex> vertices) {
        this.vertices = vertices;
    }

}
