package com.xaman.gov.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by monica on 12/29/17.
 */

public class DetectionData implements Parcelable {
    @SerializedName("image")
    @Expose
    public Image image;
    @SerializedName("requestId")
    @Expose
    public String requestId;
    @SerializedName("objects")
    @Expose
    public ArrayList<TrafficObject> objects;

    public DetectionData() {
        objects = new ArrayList<>();
    }

    protected DetectionData(Parcel in) {
        image = in.readParcelable(Image.class.getClassLoader());
        requestId = in.readString();
        objects = in.createTypedArrayList(TrafficObject.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(image, flags);
        dest.writeString(requestId);
        dest.writeTypedList(objects);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DetectionData> CREATOR = new Creator<DetectionData>() {
        @Override
        public DetectionData createFromParcel(Parcel in) {
            return new DetectionData(in);
        }

        @Override
        public DetectionData[] newArray(int size) {
            return new DetectionData[size];
        }
    };

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public ArrayList<TrafficObject> getObjects() {
        return objects;
    }

    public void setObjects(ArrayList<TrafficObject> objects) {
        this.objects = objects;
    }
}
