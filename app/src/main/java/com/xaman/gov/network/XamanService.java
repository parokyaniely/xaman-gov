package com.xaman.gov.network;

import com.xaman.gov.models.TrafficStatus;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;

/**
 * Created by monica on 12/28/17.
 */

public interface XamanService {
    @Multipart
    @POST("traffic")
    Call<TrafficStatus> updateTrafficStatus(@PartMap Map<String, RequestBody> params,
                                            @Part MultipartBody.Part file);
}
